module spikegpubsub

go 1.13

require (
	cloud.google.com/go/pubsub v1.5.0
	github.com/google/uuid v1.1.1
	github.com/jawher/mow.cli v1.1.0
)
