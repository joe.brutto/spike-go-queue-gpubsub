package configuration

import (
	"github.com/google/uuid"
	"spikegpubsub/errors"
)

// The configuration to use for launching the application.
type Configuration struct {
	SystemID  string
	ProjectID string
}

// Creates a new, basic configuration for the application.
func New(projectId string) *Configuration {
	// generate a System ID
	systemUuid, err := uuid.NewUUID()
	errors.FailOnError(err, "Unable to create System ID")
	systemId := systemUuid.String()

	// create the configuration and exit
	return &Configuration{
		SystemID:  systemId,
		ProjectID: projectId,
	}
}
