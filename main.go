package main

import (
	"github.com/jawher/mow.cli"
	"os"
	"spikegpubsub/configuration"
	"spikegpubsub/errors"
	"spikegpubsub/topic"
)

const QueueTopic = "spike-topic"
const MessageCount = 25

func main() {
	// depending on what we're doing, launch the appropriate handling
	app := cli.App("Spike: Go+Pub/Sub", "Simple application for testing Go integration with Google Cloud Pub/Sub")
	app.Spec = "-p"
	projectId := app.StringOpt("p projectId", "", "The Google Project ID that the topic was created in")

	app.Command("consume", "Consume any messages that get posted to the queue", func(cmd *cli.Cmd) {
		cmd.Action = func() {
			config := configuration.New(*projectId)
			topic.ConsumeMessages(config, QueueTopic)
		}
	})

	app.Command("produce", "Produce messages on the queue", func(cmd *cli.Cmd) {
		cmd.Action = func() {
			config := configuration.New(*projectId)
			topic.SendMessages(config, QueueTopic, MessageCount)
		}
	})

	// run the application
	err := app.Run(os.Args)
	errors.FailOnError(err, "Unable to launch the application")
}
