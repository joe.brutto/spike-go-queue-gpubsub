package topic

import (
	"cloud.google.com/go/pubsub"
	"context"
	"log"
	"spikegpubsub/configuration"
	"spikegpubsub/errors"
	"sync"
)

const ConsumptionMessageCount = 5

// Consumes and prints any messages for the given topic.
func ConsumeMessages(configuration *configuration.Configuration, topic string) {
	// connect
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, configuration.ProjectID)
	errors.FailOnError(err, "Unable to create Pub/Sub client")

	// consume some messages
	subscription := client.Subscription(topic)
	cancelableContext, cancel := context.WithCancel(ctx)
	var consumptionMutex sync.Mutex

	messageReceiveCount := 0
	err = subscription.Receive(cancelableContext, func (ctx context.Context, message *pubsub.Message) {
		consumptionMutex.Lock()
		defer consumptionMutex.Unlock()

		log.Printf("Received Message: %s", string(message.Data))
		message.Ack()

		messageReceiveCount++
		if messageReceiveCount == ConsumptionMessageCount {
			cancel()
		}
	})

	errors.FailOnError(err, "Error during message receiving")
}
