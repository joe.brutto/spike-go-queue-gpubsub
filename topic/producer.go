package topic

import (
	"cloud.google.com/go/pubsub"
	"context"
	"fmt"
	"spikegpubsub/configuration"
	"spikegpubsub/errors"
)

// Sends a bunch of messages to the queue.
func SendMessages(configuration *configuration.Configuration, topic string, messageCount int) {
	// connect
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, configuration.ProjectID)
	errors.FailOnError(err, "Unable to create Pub/Sub client")

	// connect to the topic
	topicConnection := client.Topic(topic)

	// publish messages
	for index := 0; index < messageCount; index++ {
		fmt.Printf("Sending message: %d\n", index + 1)

		messageText := fmt.Sprintf("Producer[%s], Message[%d]", configuration.SystemID, index + 1)
		message := &pubsub.Message{Data: []byte(messageText)}
		result := topicConnection.Publish(ctx, message)

		messageId, err := result.Get(ctx)
		errors.FailOnError(err, fmt.Sprintf("Error sending message %s", messageId))
	}
}
