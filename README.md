# Spike - Go+Pub/Sub

[Google Cloud](https://cloud.google.com/) is pretty awesome. There's a lot of
great options they provide, with one of them being the "queueing" solution of
[Pub/Sub](https://cloud.google.com/pubsub/). Unlike [Amazon's SQS](https://aws.amazon.com/sqs/)
offering, there are much more reasonable [limits](https://cloud.google.com/pubsub/quotas)
on *Pub/Sub* that makes it a pretty robust solution (don't get me wrong; SQS is not
bad, but Pub/Sub is better). When I'm using Google Cloud it's the obvious choice and
I need to make sure that I can quickly and easy reference my [Go](https://golang.org/)
implementation notes for this.

## Notes

  * This isn't free, but you can get your [free credits](https://cloud.google.com/free/) to try it
  * It may not be free, but it's also [cheap](https://cloud.google.com/pubsub/pricing)
  * **We only consume 5 messages at a time** - this is a pattern I follow when using [GAE](https://cloud.google.com/appengine/) with dynamic/programmatic scaling, and that's what this spike is for (see [Monitoring](https://cloud.google.com/monitoring) for how to do that)
  
## Prerequisites

  * You need a [Google Cloud Project](https://cloud.google.com/resource-manager/docs/creating-managing-projects) set up
  * You need the topic created, which is just `gcloud pubsub topics create spike-topic` (see [here][1])
  * You need to add a subscription, or things won't work, which is `gcloud pubsub subscriptions create spike-topic-subscription --topic spike-topic` (see [here][1])

## Execution

I recommend doing this in two different terminal windows/tabs. You need to run this first,
obviously:

    go build

### Consumer

This will wait forever until you give it a `Ctrl + C`:

    ./spikegpubsub -p YOUR_GOOGLE_PROJECT_ID consume
    
### Producer

    ./spikegpubsub -p YOUR_GOOGLE_PROJECT_ID produce

## Contributing

Wanna write more things?  I have no real guidlines besides these:

 * Document and comment the hell out of everything
 * You need to make [pull/merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) or I'll ignore you
 * Be patient if I don't see your PR, I get really busy and won't be able to babysit the projects too much (especially these spike repositories)
 * Don't be a douche bag

[1]: https://cloud.google.com/pubsub/docs/quickstart-client-libraries
