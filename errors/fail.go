package errors

import (
	"log"
)

// Fails the application on an error.
func FailOnError(err error, message string) {
	if err != nil {
		log.Fatalf("%s: %s", message, err)
	}
}
