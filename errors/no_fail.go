package errors

import "fmt"

// Logs an error but doesn't kill the application.
func LogError(err error, message string) {
	fmt.Println(fmt.Errorf("%s: %s", message, err))
}
